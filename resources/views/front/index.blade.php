@extends('front.layouts.master')
@section('content')
        <h1>Все товары</h1>
        @if(session()->has('warning'))
            <p class="alert alert-danger">{{session()->get('warning')}}</p>
        @endif
        <div class="row">
            @foreach($products as $product)
                @include('front.layouts.card', compact('product'))
            @endforeach
        </div>
@endsection
